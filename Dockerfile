FROM python:3.9.2-alpine

WORKDIR /home/pi/AirQ

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./application/main.py" ]