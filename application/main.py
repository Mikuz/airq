import json
import time
import asyncio
import sys
from azure.iot.device import IoTHubDeviceClient, Message
from sensor import Sensor

MSG_TXT = '{{"pm_two_five": {telemetry_pm25},"pm_ten": {telemetry_pm10}, "messageId": {msg_id}}}'

# sensor = serial.Serial('/dev/ttyUSB0')
connection_str = 'HostName=AirQualityMonitor.azure-devices.net;DeviceId=RaspberryPi;SharedAccessKey=Yvq1SugNW9Bq8iBq6eRiY2PjUVIixr9PR5J+862wRIM='

async def init_iothub_client():
    client = IoTHubDeviceClient.create_from_connection_string(connection_str)
    return client

async def main():

    client = await init_iothub_client()
    print ("IoT Hub device ready for sending messages")

    sensor = Sensor()

    while True: 
        try:
            telemetry = await sensor.readData()
            if telemetry is not None:
                message_updated = MSG_TXT.format(telemetry_pm25 = telemetry[0], telemetry_pm10 = telemetry[1], msg_id = telemetry[2])
                message = Message(message_updated)

                print("Sending msg: {}".format(message))
                client.send_message(message)
                print ("Message sent")
                
                time.sleep(15)
        except:
             print("Unknown error occured...", sys.exit())

if __name__ == "__main__":
    asyncio.run(main())