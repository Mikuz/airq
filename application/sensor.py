import serial
import time

class Sensor:

    def __init__(self):
        self.device = serial.Serial(port = '/dev/ttyUSB0', timeout = 10)
        self.msgId = 0
        self.data = []
        self.number_of_samples = 10
    
    async def readData(self):
        self.data = []
        try:
            for index in range(0,self.number_of_samples):
                read = self.device.read()
                self.data.append(read)
                time.sleep(0.5)

            pm_2_5 = int.from_bytes(b''.join(self.data[2:4]), byteorder='little') / self.number_of_samples
            pm_10 =  int.from_bytes(b''.join(self.data[4:6]), byteorder='little') / self.number_of_samples

            self.msgId += 1

            telemetry = (pm_2_5, pm_10, self.msgId)
            
        except SerialTimeoutException:
            print('Timeout while reading sensor data...')
        
        return telemetry